package com.seibel.lod;

/**
 * This file is similar to mcmod.info
 * 
 * @author James Seibel
 * @version 05-31-2021
 */
public final class ModInfo
{	
	public static final String MODID = "lod";
	public static final String MODNAME = "Levels of Detail";
	public static final String MODAPI = "LodAPI";
	public static final String VERSION = "a1.2";
}